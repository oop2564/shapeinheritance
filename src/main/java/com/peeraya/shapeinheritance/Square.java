/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class Square extends Rectangle {
    private double side;

    public Square(String name, double side) {
        super(name, side, side);
        this.side = side;
        System.out.println("Hello, I'm " + this.name);
        System.out.println("side = " + this.side);
    }
    
    @Override
    public void showArea() {
        System.out.println("Area of " + this.name + " is " + calArea());
    }
    
    @Override
    public double calArea() {
        return side * side;
    }    
}
