/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class Rectangle extends Shape {
    private double width;
    private double height;
    
    public Rectangle(String name, double height, double width) {
        super(name, 0, 0, height, width, 0);
        this.width = width;
        this.height = height;
        System.out.println("Hello, I'm " + this.name);
        System.out.println("width = " + this.width + " height = " + this.height);
    }
    
    @Override
    public void showArea() {
        System.out.println("Area of " + this.name + " is " + calArea());
    }
    
    @Override
    public double calArea() {
        return width * height;
    }
}
