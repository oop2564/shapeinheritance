/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape("shape", 0, 0, 0, 0, 0);
        shape.showArea();
        shape.BreakRange();
        
        Circle circle1 = new Circle("circle1", 3);
        circle1.showArea();
        circle1.BreakRange();
        
        Circle circle2 = new Circle("circle2", 4);
        circle2.showArea();
        circle2.BreakRange();
        
        Triangle tri = new Triangle("tri", 4, 3);
        tri.showArea();
        tri.BreakRange();
        
        Rectangle rect = new Rectangle("rect", 3, 4);
        rect.showArea();
        rect.BreakRange();
        
        Square square = new Square("square", 2);
        square.showArea();
        square.BreakRange();
        
        System.out.println("Shape is Shape: " + (shape instanceof Shape));
        System.out.println("Shape is circle1: " + (shape instanceof Circle));
        System.out.println("Shape is circle2: " + (shape instanceof Circle));
        System.out.println("Shape is tri: " + (shape instanceof Triangle));
        System.out.println("Shape is rect: " + (shape instanceof Rectangle));
        System.out.println("Shape is square: " + (shape instanceof Square));
        
        System.out.println("circle1 is Shape: " + (circle1 instanceof Shape));
        System.out.println("circle1 is circle1: " + (circle1 instanceof Circle));
        System.out.println("circle1 is circle2: " + (circle1 instanceof Circle));
        
        System.out.println("circle2 is Shape: " + (circle2 instanceof Shape));
        System.out.println("circle2 is circle1: " + (circle2 instanceof Circle));
        System.out.println("circle2 is circle2: " + (circle2 instanceof Circle));
        
        System.out.println("tri is Shape: " + (tri instanceof Shape));
        System.out.println("tri is tri: " + (tri instanceof Triangle));      
        
        System.out.println("rect is Shape: " + (rect instanceof Shape));
        System.out.println("rect is rect: " + (rect instanceof Rectangle));
        
        System.out.println("square is Shape: " + (square instanceof Shape));
        System.out.println("square is square: " + (square instanceof Square));
        shape.BreakRange();
    }
}
