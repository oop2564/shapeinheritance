/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class Shape {
    protected String name;
    protected double r;
    protected double base;
    protected double height;
    protected double width;
    protected double side;
    
    public Shape(String name, double r, double base, double height, double width, double side) {
        System.out.println("Shape created");
        this.name = name;
        this.r = r;
        this.base = base;
        this.height = height;
        this.width = width;
        this.side = side;
    }
    
    public double calArea(){
        return 0;
    }
    
    public void showArea() {
        System.out.println("Hello, I'm " + this.name);
        System.out.println("Area of " + this.name + " is " + calArea());
    }
    
    public void BreakRange() {
        System.out.println("-------------------------");
    }
}
