/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class Circle extends Shape {
    private double r;
    private double pi = 22.0/7;
    
    public Circle(String name, double r) {
        super(name, r, 0, 0, 0, 0);
        this.name = name;
        this.r = r;
        System.out.println("Hello, I'm " + this.name);
        System.out.println("r = " + r);
    }
    
    @Override
    public void showArea() {
        System.out.println("Area of " + this.name + " is " + calArea());
    }
    
    @Override
    public double calArea() {
        return pi * r * r;
    }
}
