/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.shapeinheritance;

/**
 *
 * @author Administrator
 */
public class Triangle extends Shape {
    private double base;
    private double height;
    
    public Triangle(String name, double base, double height) {
        super(name, 0, base, height, 0, 0);
        this.base = base;
        this.height = height;
        System.out.println("Hello, I'm " + this.name);
        System.out.println("base = " + this.base + " height = " + this.height);
    }
    
    @Override
    public void showArea() {
        System.out.println("Area of " + this.name + " is " + calArea());
    }
    
    @Override
    public double calArea() {
        return (1.0/2) * base * height;
    }
}
